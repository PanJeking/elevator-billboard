#include <stdio.h>
#include "object.h"

#define USR_ACCOUNT "root"
#define USR_PASSWORD "0"

//================================================================

//初始化
LcdDevice *lcd;
struct TsDevice *ts;
Point point_xy;
extern ListNode *Dirhead;
Button *buttonptr[7];
int in_flag = 0;
int login_flag = 1;
int cmpPassword_flag = 0;

ListNode *video_head;
ListNode *photo_head;
pthread_t tid_photo;
pthread_t tid_avi;

// 添加 节点进链表
bool add_photo(ListNode *head, char *video_name)
{
    ListNode *new = createlist(video_name);

    ListNode *pos = head->next;
    int n = displayNode(head);
    int i = 0;
    char buff[128] = {0};
    while (pos != head)
    {
        if (strcmp(pos->data, new->data)) //不相同
        {
            sprintf(buff, "%s", pos->data);
            printf("已进入链表有%s\n", buff);
            i++;
        }
        if (i == n)
        {
            insertNode(head, new);
            sprintf(buff, "%s", new->data);
            printf("新进入链表有%s\n", buff);
            break;
        }
        pos = pos->next;
    }
    return 0;
}

// 删除 节点从链表
bool del_photo(ListNode *head, char *photo_name)
{

    ListNode *new = createlist(photo_name);

    ListNode *pos = head->next;
    ListNode *tmp;
    char buff[128] = {0};
    while (pos != head)
    {
        if (!strcmp(pos->data, new->data)) //相同
        {
            tmp = pos->next;
            deleteNode(pos);
            pos = tmp;
        }
        else
        {
            sprintf(buff, "%s", pos->data);
            printf("已进入链表有%s\n", buff);
            pos = pos->next;
        }
    }
    return 0;
}

// 功能： 指定文件 输出到 LCD
// 参数： lcd文件描述符指针 指定文件的路径名 配置文件格式为 ↓
// button,起始x,起始y,宽w,高h,背景颜色bgcolor,文字text,文字大小,文字颜色,文字背景图片.bmp
// button,0,60,800,60,0xffffffff,车站广告系统,60,0x0,test.bmp
void init_login(LcdDevice *lcd, char *document)
{
    //打开ui文件
    FILE *file = fopen(document, "r+");
    if (file == NULL)
        return;

    int x, y, w, h;
    unsigned int bgcolor, color;
    int fontsize;
    char text[32];
    char picture[32];
    char linebuf[256] = {0};

    char *point_xy = NULL;
    int i = 0;
    while (point_xy = fgets(linebuf, 256, file))
    {

        sscanf(linebuf, "button,%d,%d,%d,%d,%x,%[^,],%d,%x,%s",
               &x, &y, &w, &h, &bgcolor, text, &fontsize, &color, picture);

        Button *bt = button_create(w, h);
        button_move(bt, x, y);
        button_set_text(bt, text);
        button_set_background(bt, bgcolor);
        button_set_fontsize(bt, fontsize);
        button_set_color(bt, color);
        button_show(lcd, bt);
        // button_destroy(bt);
        buttonptr[i++] = bt;
        memset(text, 0, 32);
    }

    fclose(file);
}

// 功能： 去指定文件中获取字符串
// 参数： 指定文件的路径名
// 返回： 获取的字符串的首地址(在堆空间)
char *sprintf_rolltext_str(char *document)
{
    //打开ui文件
    FILE *file = fopen(document, "r+");
    if (file == NULL)
        return NULL;

    // linebuf申请堆空间，里面是打包的字符串
    char *linebuf = malloc(256);

    fgets(linebuf, 256, file);

    fclose(file);

    return linebuf;
}

//============================================================================
// 图片线程 视频链表
// 线程内的弹栈释放
void *photoShow_tasks_deleteNode(void *arg)
{
    // deleteNode((ListNode *)arg);
}
// 每秒显示一张图片的线程（从9到0循环播放）
void *photoShow_tasks(void *arg)
{
    printf("photoShow_tasks start\n");

    // 压栈 以防 本线程 暴毙 导致 button_create没有button_destory
    // pthread_cleanup_push((void *)photoShow_tasks_deleteNode,
    //                      (void *)photo_head);

    // 临时指针，用来指向photo_head链表的每一个节点
    ListNode *tmp = photo_head->next;
    // 遍历显示链表中的图片
    while (1)
    {
        // printf("%s\n", tmp->data);

        BmpImage *bmp = bmp_create(tmp->data);
        bmp_reversal(bmp);
        bmp_zoom(bmp, 200, 360);
        lcd_show_bmp(lcd, 600, 60, bmp->image, 200, 360);
        sleep(1);

        tmp = tmp->next;

        // 当tmp循环指向头节点，跳过头节点（因为photo_head->data=NULL）
        if (tmp == photo_head)
        {
            tmp = photo_head->next;
        }
    }
    // 弹栈 为0时，执行 压栈的函数 后 结束本线程
    //     为1时，不执行 直接 结束本线程
    // pthread_cleanup_pop(1);
    printf("photoShow_tasks 正常 end\n");
}

// 时间线程
// 线程内的弹栈释放
void *tasks_button_destroy(void *arg)
{
    button_destroy((Button *)arg);
    // printf("销毁按钮函数 弹栈成功\n");
}
void *timeShow_tasks(void *arg)
{
    time_t timep;
    char *time_p;
    Button *time_font = button_create(370, 60);
    button_move(time_font, 430, 0);
    button_set_background(time_font, 0xffffffff);
    button_set_fontsize(time_font, 30);
    button_set_color(time_font, 0x0);

    // 压栈 以防 本线程 暴毙 导致 button_create没有button_destory
    // 参数：要压栈的函数 要压栈函数的参数
    pthread_cleanup_push((void *)tasks_button_destroy,
                         (void *)time_font);

    while (1)
    {
        time(&timep);
        time_p = ctime(&timep);
        button_set_text(time_font, time_p);
        button_show(lcd, time_font);
        sleep(1);
    }

    // 弹栈 为0时，执行 压栈的函数 后 结束本线程
    //     为1时，不执行 直接 结束本线程
    pthread_cleanup_pop(1);
}

// 滚动字幕线程
void *rollText_tasks(void *arg)
{
    char *rolltext_str = sprintf_rolltext_str("./rollText.ui");

    Button *rolltext = button_create(800, 40);
    button_move(rolltext, 0, 440);
    button_set_text(rolltext, rolltext_str);
    button_set_background(rolltext, 0xffffffff);
    button_set_fontsize(rolltext, 40);
    button_set_color(rolltext, 0x0);

    // 压栈 以防 本线程 暴毙 导致 button_create没有button_destory
    pthread_cleanup_push((void *)tasks_button_destroy,
                         (void *)rolltext);
    int font_pos_x = 800;
    while (1)
    {
        button_show_x_y(lcd, rolltext, font_pos_x, 0);
        font_pos_x -= 5;
        usleep(500);
        if (font_pos_x == -1 * (strlen(rolltext_str) * 20))
        {
            font_pos_x = 800;
        }
    }

    // 弹栈 为0时，执行 压栈的函数 后 结束本线程
    //     为1时，不执行 直接 结束本线程
    pthread_cleanup_pop(1);
}

// 销毁链表的压栈函数
void *pthread_cleanup_push_video(void *arg)
{
    system("killall -9 mplayer"); //退出当前播放器
    // ListNode *video_head = (ListNode *)arg;
    // destroyNode(video_head); //等价 ↓
    // deleteNode((ListNode *)arg);
    printf("pthread_cleanup_push_video\n");
}

// 功能：点击按钮 摸到指定位置 将触摸点归零 返回1
// 参数：按钮长度 按钮宽度
int click_button(int x1, int x2, int y1, int y2)
{
    // 摸到指定位置 将触摸点归0 返回1
    if (point_xy.x > x1 * 1024 / 800 && point_xy.x < x2 * 1024 / 800 && point_xy.y > y1 * 600 / 480 && point_xy.y < y2 * 600 / 480)
    {
        point_xy.x = point_xy.y = 0;
        return 1;
    }
    // 没摸到返回0
    return 0;
}
// 视频 切换 暂停 线程
void *video_task(void *arg)
{
    pthread_detach(pthread_self()); //线程自已设置自已的可分离属性
    printf("video_task start\n");
    // 视频 开始/暂停 标志
    BmpImage *stop = bmp_create("./video_photo/stop.bmp"); //暂停视频图标
    int video_flag = 1;
    bmp_reversal(stop);
    bmp_zoom(stop, 50, 50);

    // 压栈 以防 本线程 暴毙 导致 button_create没有button_destory
    // 参数：要压栈的函数 要压栈函数的参数
    pthread_cleanup_push((void *)pthread_cleanup_push_video,
                         NULL);

    // 临时指针，用来指向video_head链表的每一个节点
    ListNode *tmp_video = video_head->next;
    printf("%s\n", tmp_video->data);

    // 一进入主界面 就播放第一个视频
    char video_system[128];
    sprintf(video_system, "mplayer -quiet -geometry 0:60 -zoom -x 600 -y 360 %s &", tmp_video->data);
    system(video_system);
    printf("正在播放第一个视频\n");
    // 等待用户 操作 视频按钮
    while (1)
    {
        // 触摸切换上一个视频
        if (click_button(60, 120, 0, 60) == 1)
        {
            if (tmp_video->prev == video_head)
                tmp_video = video_head->prev;
            else
                tmp_video = tmp_video->prev;
            system("killall -9 mplayer"); //退出当前播放器
            printf("正在播放上一个视频%s\n", tmp_video->data);
            sprintf(video_system, "mplayer -quiet -geometry 0:60 -zoom -x 600 -y 360 %s &", tmp_video->data);
            system(video_system);
        }

        // 触摸切换下一个视频
        if (click_button(120, 180, 0, 60) == 1)
        {
            if (tmp_video->next == video_head)
                tmp_video = video_head->next;
            else
                tmp_video = tmp_video->next;
            system("killall -9 mplayer"); //退出当前播放器
            printf("正在播放下一个视频%s\n", tmp_video->data);
            sprintf(video_system, "mplayer -quiet -geometry 0:60 -zoom -x 600 -y 360 %s &", tmp_video->data);
            system(video_system);
        }

        // 暂停/播放
        if (click_button(0, 600, 60, 400) == 1)
        {

            if (video_flag == 1)
            {
                system("killall -19 mplayer");                    //让播放器暂停播放
                lcd_show_bmp(lcd, 300, 190, stop->image, 50, 50); //显示暂停图标
                printf("视频已暂停\n");
                video_flag = 0;
            }
            else
            {
                system("killall -18 mplayer"); //让播放器继续播放
                printf("视频正播放\n");
                video_flag = 1;
            }
        }

        // system("killall -9 mplayer");  //让播放器退出
    }
    // 弹栈 为0时，执行 压栈的函数 后 结束本线程
    //     为1时，不执行 直接 结束本线程
    pthread_exit(NULL);
    pthread_cleanup_pop(1);
}

// 选择 添加/删除 线程
void *choose_task(void *arg)
{
    while (1)
    {
        // 按下 图片按钮 显示图片列表
        if (click_button(180, 240, 0, 60))
        {
            // 1.退出本界面就关闭显示 图片 的线程
            pthread_cancel(tid_photo);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_photo, NULL);
            printf("通过触摸文件图标关闭显示 图片 的线程\n");

            while (1)
            {
                // 一直获取坐标
                point_xy = ts_point(ts);
                // 添加
                if (click_button(300, 360, 0, 60))
                {
                    displayNode(photo_head);
                    printf("正在添加图片\n");
                    char photo_name[128];
                    scanf("%s", photo_name);
                    add_photo(photo_head, photo_name);
                    displayNode(photo_head);
                    break;
                }
                // 删除
                if (click_button(360, 420, 0, 60))
                {
                    displayNode(photo_head);
                    printf("正在删除图片\n");
                    char photo_name[128];
                    scanf("%s", photo_name);
                    del_photo(photo_head, photo_name);
                    displayNode(photo_head);
                    break;
                }
            }

            // 1.创建遍历图片的线程
            pthread_create(&tid_photo, NULL, (void *)photoShow_tasks, NULL);
        }

        // 按下 视频按钮 显示视频列表
        if (click_button(240, 360, 0, 60))
        {
            // 1.退出本界面就关闭显示 视频 的线程
            pthread_cancel(tid_avi);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_avi, NULL);
            system("killall -9 mplayer");
            printf("通过触摸视频图标关闭显示 视频 的线程\n");

            while (1)
            {
                // 一直获取坐标
                point_xy = ts_point(ts);
                // 添加
                if (click_button(300, 360, 0, 60))
                {
                    displayNode(video_head);
                    printf("正在添加视频\n");
                    char video_name[128];
                    scanf("%s", video_name);
                    add_photo(video_head, video_name);
                    displayNode(video_head);
                    break;
                }
                // 删除
                if (click_button(360, 420, 0, 60))
                {
                    displayNode(video_head);
                    printf("正在删除视频\n");
                    char video_name[128];
                    scanf("%s", video_name);
                    del_photo(video_head, video_name);
                    displayNode(video_head);
                    break;
                }
            }
            // 1.创建遍历视频的线程
            pthread_create(&tid_avi, NULL, (void *)video_task, NULL);
        }
    }
}

// 主界面
void main_window(void)
{
    // 清屏
    lcd_clear(lcd, 0xffffff);

    // 显示 返回按钮 图片
    bmp_show(lcd, "./video_photo/return.bmp", 0, 0, 60, 60);
    // 显示 上一个按钮 图片
    bmp_show(lcd, "./video_photo/left.bmp", 60, 0, 55, 60);
    // 显示 下一个按钮 图片
    bmp_show(lcd, "./video_photo/right.bmp", 120, 0, 55, 60);
    // 显示 图片按钮
    bmp_show(lcd, "./video_photo/photo.bmp", 180, 0, 60, 60);
    // 显示 视频按钮 图片
    bmp_show(lcd, "./video_photo/play.bmp", 240, 0, 60, 60);
    // 显示 添加按钮 图片
    bmp_show(lcd, "./video_photo/add.bmp", 300, 0, 60, 60);
    // 显示 删除按钮 图片
    bmp_show(lcd, "./video_photo/del.bmp", 360, 0, 60, 60);

    // 1.创建遍历图片的线程
    int ret = pthread_create(&tid_photo, NULL, (void *)photoShow_tasks, NULL);
    if (ret < 0)
    {
        perror("pthread_create");
    }

    // 2.创建显示时间线程
    pthread_t tid_time;
    int ret_time = pthread_create(&tid_time, NULL, (void *)timeShow_tasks, NULL);
    if (ret_time < 0)
    {
        perror("pthread_create");
    }

    // 3.创建滚动字幕线程
    pthread_t tid_text;
    int ret_text = pthread_create(&tid_text, NULL, (void *)rollText_tasks, NULL);
    if (ret_text < 0)
    {
        perror("pthread_create");
    }

    // 4.创建视频线程
    int ret_avi = pthread_create(&tid_avi, NULL, (void *)video_task, NULL);
    if (ret_avi < 0)
    {
        perror("pthread_create");
    }

    // 5.创建选择线程
    pthread_t tid_choose;
    int ret_choose = pthread_create(&tid_choose, NULL, (void *)choose_task, NULL);
    if (ret_choose < 0)
    {
        perror("pthread_create");
    }

    // 最后:等待"返回"
    while (1)
    {
        // 一直获取坐标
        point_xy = ts_point(ts);

        // 按"返回"
        if (click_button(0, 60, 0, 60))
        {
            // 1.退出本界面就关闭显示 图片 的线程
            pthread_cancel(tid_photo);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_photo, NULL);
            printf("1.退出主界面就关闭显示 图片 的线程\n");

            // 2.退出本界面就关闭显示 时间 的线程
            pthread_cancel(tid_time);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_time, NULL);
            printf("2.退出主界面就关闭显示 时间 的线程\n");

            // 3.退出本界面就关闭显示 滚动字幕 的线程
            pthread_cancel(tid_text);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_text, NULL);
            printf("3.退出主界面就关闭显示 滚动字幕 的线程\n");

            // 4.退出本界面就关闭 视频 的线程和视频
            pthread_cancel(tid_avi);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            // pthread_join(tid_avi, NULL);这句话死循环

            system("killall -9 mplayer");
            printf("4.退出主界面就关闭 视频 的线程和 视频 \n");
            // 5.退出本界面就关闭关闭 选择 的线程
            pthread_cancel(tid_choose);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            // pthread_join(tid_choose, NULL);//死循环
            printf("5.退出主界面就关闭 选择 的线程\n");
            destroyNode(photo_head);
            destroyNode(video_head);

            // 一旦 返回
            // 将判断进入主界面的标志位in_flag = 0
            // 将判断进入输入账号密码的标志位login_flag = 1
            in_flag = 0;
            login_flag = 1;
            cmpPassword_flag = 0;
            break;
        }
    }
}
// 登陆界面
void show_login(void)
{
    // 清屏
    lcd_clear(lcd, 0xffffff);
    // 登陆界面
    init_login(lcd, "./login.ui");
    // boder
    lcd_draw_rect(lcd, 370, 200, 200, 40, 0x0);
    lcd_draw_rect(lcd, 370, 250, 200, 40, 0x0);
}

/************ 键盘输入用户密码相关的函数 *****************/
// 功能：将字符，拼接到字符串中，并显示到LCD上,有 backspace 删除功能,flag是否打开隐藏密码功能
static char str_1[32] = {0}; //存'*'的字符串数组
void ch_to_str_show(char ch, char *str, int x, int y, int flag)
{
    // 保存用户输入的字符，打包成 账号 字符串
    // 如果一开始就按 "backspace" 就不打包进str字符串
    if (ch != '\b')
    {
        sprintf(str, "%s%c", str, ch);
        // 当输入 密码(flag==true) 时，str_1会存入'*',作为 显示 的输出,与str同步操作
        if (flag == true)
        {
            sprintf(str_1, "%s%c", str_1, '*');
        }
    }
    printf("\n显示当前输入内容:%s", str);
    // 按 "backspace" 删除最后一个字符
    if (ch == '\b')
    {
        // 删除str最后一个字符
        str[strlen(str) - 1] = '\0';
        if (flag == true)
        {
            str_1[strlen(str_1) - 1] = '\0';
        }

        // 显示删除后的字符串
        if (flag == false)
        {
            show_font(str, x, y, 200 - 2, 40 - 2, 30, 0x0, 0xffffffff, true, 5, 4, lcd);
        }
        else
        {
            show_font(str_1, x, y, 200 - 2, 40 - 2, 30, 0x0, 0xffffffff, true, 5, 4, lcd);
        }
    }
    // 显示打包好的用户 账号 字符串
    if (flag == false)
    {
        show_font(str, x, y, 200 - 2, 40 - 2, 30, 0x0, 0xffffffff, true, 5, 4, lcd);
    }
    else
    {
        show_font(str_1, x, y, 200 - 2, 40 - 2, 30, 0x0, 0xffffffff, true, 5, 4, lcd);
    }
}

// 功能：用户输入 账号 密码,直到用户输入的账号密码正确才能退出本函数
// 参数：无
void usr_input(void)
{
    // 存放用户输入的一个字符
    char ch;
    // 存放用户的账号或密码的字符串数组
    char usr_Account[32] = {0};
    char usr_Password[32] = {0};
    printf("请输入账号:\n");
    // 输入账号的死循环
    while (1)
    {
        // 获取用户输入的字符
        scanf("%c", &ch);

        // 用户输入 "回车" 或 "Tab" 转到 密码行
        if (ch == '\n' || ch == '\t')
        {
            printf("请输入密码:\n");
            // 输入密码的死循环
            while (1)
            {
                scanf("%c", &ch);

                // 将用户输入的一个字符存入到字符串之前，先判断输入的是否是回车，如果是就继续比较账号密码是否正确
                if (ch == '\n')
                {
                    // 比较账号密码是否正确，如果正确就return直接退出函数，否则跳到最外层的while循环
                    if (!strcmp(usr_Account, USR_ACCOUNT) && !strcmp(usr_Password, USR_PASSWORD))
                    {

                        printf("**账号密码** 正确\n");
                        in_flag = 1;
                        return;
                    }
                    else
                    {
                        // 显示 账号或密码错误 提示
                        show_font("账号或密码 错误!!!", 300, 330, 200, 20, 20, 0x00ff00, 0xffffffff, false, 0, 0, lcd);
                        printf("**账号密码** 错误\n");
                        printf("**请重新输入** \n");

                        // 将内存中的每个字节 赋 一个值
                        memset(usr_Account, ' ', 31);
                        memset(usr_Password, ' ', 31);

                        // 清除 账号密码输入框
                        ch_to_str_show(' ', usr_Account, 371, 201, false);
                        ch_to_str_show(' ', usr_Password, 371, 251, false);

                        // 跳到最外层的while循环之前先清空 usr_Account、usr_Password、显示'*'的str_1数组 的内存
                        bzero(usr_Account, 32);
                        bzero(usr_Password, 32);
                        bzero(str_1, 32);

                        break;
                    }
                }
                else
                {
                    // 将用户键盘输入的 密码 字符，拼接到字符串中，并显示到LCD上
                    ch_to_str_show(ch, usr_Password, 371, 251, true);

                    // 比较账号密码是否正确，如果正确就return直接退出函数，否则跳到最外层的while循环
                    if (!strcmp(usr_Account, USR_ACCOUNT) && !strcmp(usr_Password, USR_PASSWORD))
                    {
                        // printf("\n**账号密码** 正确\n");
                        cmpPassword_flag = 1; // 只有在 账号密码 正确 开启触摸登录
                    }
                }
            }
        }
        else
        {
            // 将用户键盘输入的 账号 字符，拼接到字符串中，并显示到LCD上
            ch_to_str_show(ch, usr_Account, 371, 201, false);
            // 刷白 错误 提示
            show_font("账号或密码 错误!!!", 300, 330, 200, 20, 20, 0xffffffff, 0xffffffff, false, 0, 0, lcd);
        }
    }
}

// 输入账号密码的线程
void *login_task(void *arg)
{
    printf("login_task start\n");

    pthread_detach(pthread_self()); //线程自已设置自已的可分离属性
    // 修改终端的输入模式为单缓冲(不需要按回车就可以获取键盘键入内容)
    terminal_notEnter_mode();
    // 用户输入 账号 密码
    usr_input();
    // 修改终端的输入模式为行缓冲（需要按回车才能获取到标准输入中的内容）
    terminal_Enter_mode();
    // 线程退出
    printf("login_task end\n");
}
/********************  END  ***********************/

void *touch_Enter_task(void *arg)
{
    printf("touch_Enter_task start\n");
    pthread_detach(pthread_self()); //线程自已设置自已的可分离属性

    // 卡住 等待键盘输入 账号密码
    while (cmpPassword_flag == 0)
    {
    }
    ts = ts_init("/dev/input/event0"); // 只有在 触摸登录 才开启触摸板
    while (1)
    {
        // 显示触摸坐标-已换算
        point_xy = ts_point(ts);
        // 按"登录"
        if (point_xy.x > 260 * 1024 / 800 && point_xy.x < 360 * 1024 / 800 && point_xy.y > 380 * 600 / 480 && point_xy.y < 420 * 600 / 480)
        {
            printf("通过触摸登录键的touch_Enter_task end\n");
            in_flag = 1;
            pthread_exit(NULL);
        }
        // 通过 账户密码正确 输入回车进入的方式成功
        if (in_flag == 1)
        {
            printf("因为输入密码正确的touch_Enter_task end\n");
            pthread_exit(NULL);
        }
    }
}

//============================================================================
int main(void)
{

    lcd = lcd_init("/dev/fb0");

    // 登陆界面
    show_login();

    pthread_t tid_touch_Enter;
    pthread_t tid_login;

    while (1)
    {
        // 可以运行第一次 输入账号密码的 线程
        if (login_flag == 1)
        {
            // 一进来直接关闭  输入账号密码的 标志位 防止无限创建
            login_flag = 0;
            /**********************   账户密码 线程  *************************/
            // 创建一个输入账号密码的线程
            int ret_login = pthread_create(&tid_login, NULL, (void *)login_task, NULL);
            if (ret_login < 0)
            {
                perror("创建一个输入账号密码的线程pthread_create");
            }
            // 创建一个 判断 触摸登录键 进入主界面 的线程
            int ret_touch_Enter = pthread_create(&tid_touch_Enter, NULL, (void *)touch_Enter_task, NULL);
            if (ret_touch_Enter < 0)
            {
                perror("创建一个判断触摸登录键进入主界面的线程pthread_create");
            }

            // 以上两个线程，账户密码正确就会让 in_flag = 1
        }
        /**************************   END    *********************************/
        // 循环判断是否能进入主界面
        if (in_flag)
        {
            // 1.进入主界面就退出输入账号密码的线程
            pthread_cancel(tid_login);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_login, NULL);
            printf("1.进入主界面login_task 正常 end\n");

            // 2.进入主界面就退出判断 触摸登录键 进入主界面 的线程
            pthread_cancel(tid_touch_Enter);
            // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
            pthread_join(tid_touch_Enter, NULL);
            printf("2.进入主界面touch_Enter_task 正常 end\n");
            printf("进入主界面就关闭 账号密码输入 的线程\n");

            // 进入主界面前先清除登录框
            lcd_draw_rect(lcd, 370, 200, 200, 40, ~0x0);
            lcd_draw_rect(lcd, 370, 250, 200, 40, ~0x0);

            photo_head = createlist(NULL);
            // 遍历图片目录
            search_dir("./photo", ".bmp", photo_head);
            // 遍历视频目录
            video_head = createlist(NULL);
            search_dir("./video", ".avi", video_head);
            // 进入主界面 退出时将in_flag=0 将login_flag = 1;
            main_window();

            // 返回登陆界面
            show_login();
        }
    }
    // 释放
    lcd_destroy(lcd);

    return 0;
}