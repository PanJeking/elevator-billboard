CC=arm-linux-gcc
TARGET=project
SRCS=$(wildcard *.c */*.c)
OBJS=$(patsubst %.c,%.o,$(SRCS)) #所有.c文件文件名称里面的.c用.o替换

$(TARGET):$(OBJS)
	$(CC) -o $@  $^  -L./lib-arm -ljpeg -lm -lpthread
%.o:%.c
	$(CC)  -o $@ -c $<  -I./inc 

libmy.so:
	gcc -fPIC -shared -o ./lib/libmy.so	$^

dbg:
	gcc  -g *.c -o project.dgb -I./inc -lpthread

clean:
	rm -f $(TARGET)  $(OBJS)
	make

cp:
	cp project /home/jeking/nfs/