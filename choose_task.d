#ifndef _CHOOSE_TASK_C
#define _CHOOSE_TASK_C

#include "./inc/object.h"

extern

    void *
    choose_task(void *arg)
{
    // 按下 图片按钮 显示图片列表
    if (click_button(180, 240, 0, 60))
    {
        // 1.退出本界面就关闭显示 图片 的线程
        pthread_cancel(tid_photo);
        // 关闭线程后手动接合线程（表示结束线程后回收线程的资源）
        pthread_join(tid_photo, NULL);
        printf("通过触摸文件图标关闭显示 图片 的线程\n");

        while (1)
        {
            // 一直获取坐标
            point_xy = ts_point(ts);
            // 添加
            if (click_button(300, 360, 0, 60))
            {
                displayNode(photo_head);
                printf("正在添加图片\n");
                char photo_name[128];
                scanf("%s", photo_name);
                add_photo(photo_head, photo_name);
                displayNode(photo_head);
                break;
            }
            // 删除
            if (click_button(360, 420, 0, 60))
            {
                displayNode(photo_head);
                printf("正在删除图片\n");
                char photo_name[128];
                scanf("%s", photo_name);
                del_photo(photo_head, photo_name);
                displayNode(photo_head);
                break;
            }
        }

        // 1.创建遍历图片的线程
        pthread_create(&tid_photo, NULL, (void *)photoShow_tasks, NULL);
    }
}
#endif