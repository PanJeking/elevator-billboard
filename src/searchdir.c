#include "object.h"
//功能：检索指定 类型 的 文件
//参数：检索路径(字符串) 检索类型(字符串) 链表头指针
//返回：NULL

void search_dir(const char *dname, const char *type, ListNode *head)
{
    //打开目录
    DIR *dir = opendir(dname);
    if (dir == NULL)
    {
        perror("opendir");
        return;
    }
    //定义目录项指针
    struct dirent *dret = NULL;
    while (dret = readdir(dir))
    {
        if (dret->d_type == DT_DIR && strcmp(dret->d_name, ".") != 0 && strcmp(dret->d_name, "..") != 0)
        {
            //打开目录，读目录， 关闭目录
            //打开目录路径
            char path[512] = {0};
            sprintf(path, "%s/%s", dname, dret->d_name);
            //递归遍历目录
            search_dir(path, type, head);
        }
        else if (strstr(dret->d_name, type))
        {
            // 链表
            char path[512] = {0};
            sprintf(path, "%s/%s", dname, dret->d_name);
            // 创建链表节点-存本次找到的文件名
            ListNode *node = createlist(path);
            printf("链表节点文件名 %s \n", path);
            // 新节点插入链表
            insertNode(head, node);
        }
    }
    //关闭目录
    closedir(dir);
}