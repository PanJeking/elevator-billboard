#include "object.h"

// 功能：将文字 上下居中，左右居中 显示到lcd
// 参数：lcd描述符 画板指针 文字横坐标
void button_show(LcdDevice *lcd, Button *button)
{
    if (lcd == NULL || button == NULL)
    {
        perror("button_show");
        return;
    }

    //绘制背景颜色
    // lcd_draw_full(lcd, button->x, button->y, button->w, button->h, button->background);
    //显示文字
    if (button->text != NULL)
    {
        font *f = fontLoad("simfang.ttf");
        bitmap *bm = NULL;
        //判断是否有背景图片--就用图片作背景
        if (button->image != NULL)
        {
            BmpImage *bmp = bmp_create(button->image);
            bmp_zoom(bmp, button->w, button->h);
            bmp_reversal(bmp);
            bm = (bitmap *)bmp;
        }
        else
        {                                                                           //如果没有图片就用背景颜色
            bm = createBitmapWithInit(button->w, button->h, 3, button->background); // RGBA
        }

        fontSetSize(f, button->fontsize);

        int ch = 0, zh = 0;
        char *s = button->text;
        //汉字二进制最高位为1
        for (int i = 0; i < strlen(s); i++)
        {
            if (s[i] < 127 && s[i] > 0)
                ch++;
            else
            {
                zh++;
                i += 2;
            }
        }
        int fx = (button->w - (zh * button->fontsize + ch * button->fontsize / 2)) / 2; //计算文字绘制的位置
        int fy = (button->h - button->fontsize) / 2;

#if 0
        int fx = (button->w-(strlen(button->text))/3*button->fontsize)/2;//计算文字绘制的位置
        int fy = (button->h-button->fontsize)/2;
        if(strlen(button->text) <= 2)
        {
            //绘制数组和字母（一个字母占用fontsize一半）
            fx = (button->w-strlen(button->text)*button->fontsize/2)/2;
        }
#endif
        fontPrint(f, bm, fx, fy, button->text, button->color, 0);
        fontUnload(f);
        //显示在lcd上
        lcd_show_font(lcd, button->x, button->y, bm->map, button->w, button->h);
        destroyBitmap(bm);
    }
    else
    {
        if (button->image != NULL)
        {
            BmpImage *bmp = bmp_create(button->image);
            bmp_zoom(bmp, button->w, button->h);
            bmp_reversal(bmp);
            lcd_show_font(lcd, button->x, button->y, bmp->image, button->w, button->h);
            bmp_destroy(bmp);
        }
    }
}

// 功能：将文字 上下居中，左右自定义 显示到lcd
// 参数：lcd描述符 画板指针 文字横坐标
// 左右居中
// x = (button->w - (zh*button->fontsize + ch * button->fontsize / 2))/2;
// 上下居中
// y = (button->h - button->fontsize) / 2;
void button_show_x_y(LcdDevice *lcd, Button *button, int font_pos_x, int font_pos_y)
{
    if (lcd == NULL || button == NULL)
    {
        perror("button_show");
        return;
    }

    //绘制背景颜色
    // lcd_draw_full(lcd, button->x, button->y, button->w, button->h, button->background);
    //显示文字
    if (button->text != NULL)
    {
        font *f = fontLoad("simfang.ttf");
        bitmap *bm = NULL;
        //判断是否有背景图片--就用图片作背景
        if (button->image != NULL)
        {
            BmpImage *bmp = bmp_create(button->image);
            bmp_zoom(bmp, button->w, button->h);
            bmp_reversal(bmp);
            bm = (bitmap *)bmp;
        }
        else
        {                                                                           //如果没有图片就用背景颜色
            bm = createBitmapWithInit(button->w, button->h, 3, button->background); // RGBA
        }

        fontSetSize(f, button->fontsize);

        int ch = 0, zh = 0;
        char *s = button->text;
        //汉字二进制最高位为1
        for (int i = 0; i < strlen(s); i++)
        {
            if (s[i] < 127 && s[i] > 0)
                ch++;
            else
            {
                zh++;
                i += 2;
            }
        }
        int fx = (button->w - (zh * button->fontsize + ch * button->fontsize / 2)) / 2; //计算文字绘制的位置
        int fy = (button->h - button->fontsize) / 2;

#if 0
        int fx = (button->w-(strlen(button->text))/3*button->fontsize)/2;//计算文字绘制的位置
        int fy = (button->h-button->fontsize)/2;
        if(strlen(button->text) <= 2)
        {
            //绘制数组和字母（一个字母占用fontsize一半）
            fx = (button->w-strlen(button->text)*button->fontsize/2)/2;
        }
#endif
        fontPrint(f, bm, font_pos_x, font_pos_y, button->text, button->color, 0);
        fontUnload(f);
        //显示在lcd上
        lcd_show_font(lcd, button->x, button->y, bm->map, button->w, button->h);
        destroyBitmap(bm);
    }
    else
    {
        if (button->image != NULL)
        {
            BmpImage *bmp = bmp_create(button->image);
            bmp_zoom(bmp, button->w, button->h);
            bmp_reversal(bmp);
            lcd_show_font(lcd, button->x, button->y, bmp->image, button->w, button->h);
            bmp_destroy(bmp);
        }
    }
}

// 功能：使用画板显示文字
// 参数：文字内容 起始坐标(x,y) 画板宽高 文字大小 文字颜色 文字背景颜色 是否开启居中自定义(如果为false,后两个参数随意填写) 文字相对于画板的坐标x(左右居中请用button_show) 文字相对于画板的坐标y(上下居中y=(画板高-文字大小)/2) LCD显示屏句柄
void show_font(char *text,
               int x, int y,
               int w, int h,
               int font_size, int font_color,
               int bgcolor,
               int flag,
               int font_pos_x, int font_pos_y,
               LcdDevice *lcd)
{
    Button *bt = button_create(w, h);
    button_set_text(bt, text);
    button_move(bt, x, y);
    button_set_fontsize(bt, font_size);
    button_set_color(bt, font_color);
    button_set_background(bt, bgcolor);
    if (flag == true)
    {
        button_show_x_y(lcd, bt, font_pos_x, font_pos_y);
    }
    else
    {
        button_show(lcd, bt);
    }
}

// 功能：显示处理过的图片到lcd屏幕
// 参数：lcd文件描述符 文件路径 起始坐标 图片长宽
void bmp_show(LcdDevice *lcd, const char *bmpfile, int x, int y, int w, int h)
{
    BmpImage *bmp = bmp_create(bmpfile);
    bmp_reversal(bmp);
    bmp_zoom(bmp, w, h);
    lcd_show_bmp(lcd, x, y, bmp->image, w, h);
}