#include "tsevent.h"

// 获取触摸屏坐标的开关(默认开启)
int ts_flag = 1;

struct TsDevice *ts_init(const char *devname)
{
    struct TsDevice *ts = malloc(sizeof(struct TsDevice));
    ts->fd = open(devname, O_RDWR);
    if (ts->fd < 0)
    {
        perror("open");
        free(ts);
        return NULL;
    }
    return ts;
}

bool ts_destroy(struct TsDevice *ts)
{
    if (ts == NULL)
        return false;
    close(ts->fd);
    free(ts);
}
Point ts_point(struct TsDevice *ts) //获取坐标点
{
    Point point = {0, 0};
    while (1)
    {
        if (ts_flag == 0)
            return point;

        int ret = read(ts->fd, &ts->inputData, sizeof(struct input_event));
        if (ret < 0)
        {
            perror("read");
            printf("获取坐标点 error\n");
            return point;
        }

        // 判断触摸事件状态的分割线(触摸或者松开都会显示)
        // if (ts->inputData.type == EV_SYN)
        // {
        //     printf("-------- SYN --------\n");
        // }

        // 判断触摸屏是否被触摸，触摸显示 on ，松开显示 off
        // if (ts->inputData.type == EV_KEY && ts->inputData.code == BTN_TOUCH)
        // {
        //     printf("%s\n", ts->inputData.value == 0 ? "off" : "on");
        // }

        if (ts->inputData.type == EV_ABS && ts->inputData.code == ABS_X)
        {
            point.x = ts->inputData.value;
            // 显示触摸位置的 x 坐标
            // printf("x: %d\n", ts->inputData.value * 800 / 1024);
        }
        if (ts->inputData.type == EV_ABS && ts->inputData.code == ABS_Y)
        {
            point.y = ts->inputData.value;
            // 显示触摸位置的 y 坐标
            // printf("y: %d\n", ts->inputData.value * 480 / 600);
        }
        /*
        if(ts->inputData.type==EV_KEY && ts->inputData.code==BTN_TOUCH && ts->inputData.value==0)
        {
            return point;
        }*/
        if (point.x > 0 && point.y > 0)
        {
            printf("获取坐标点 ok\n");
            return point;
        }
    }
}