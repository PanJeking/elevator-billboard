#include "terminal_enter.h"

// 修改终端的输入模式为行缓冲（需要按回车才能获取到标准输入中的内容）
void terminal_Enter_mode(void)
{
    struct termios termios_p;
    bzero(&termios_p, sizeof(struct termios));

    tcgetattr(stdin->_fileno, &termios_p); // 获取终端的xxx属性
    printf("设置前 termios_p.c_lflag = %d \n", termios_p.c_lflag);
    termios_p.c_lflag |= (ICANON); // 修改属性，使用行缓冲
    printf("设置后 termios_p.c_lflag = %d \n", termios_p.c_lflag);
    tcsetattr(stdin->_fileno, TCSANOW, &termios_p); // 设置终端的xxx属性
}

// 修改终端的输入模式为单缓冲(不需要按回车就可以获取键盘键入内容)
void terminal_notEnter_mode(void)
{
    struct termios termios_p;
    bzero(&termios_p, sizeof(termios_p));
    // tc get attr获取终端 xxx 属性
    tcgetattr(stdin->_fileno, &termios_p);
    printf("设置前 termios_p.c_lflag = %d \n", termios_p.c_lflag);
    // 修改终端属性，屏蔽整行缓冲
    termios_p.c_lflag &= (~ICANON);
    printf("设置后 termios_p.c_lflag = %d \n", termios_p.c_lflag);
    // tc set attr设置终端的 xxx 属性
    tcsetattr(stdin->_fileno, TCSANOW, &termios_p);
}