#include "button.h"
//创建按钮
Button* button_create(int w, int h){
    Button *button= malloc(sizeof(Button));//申请空间
    memset(button, 0, sizeof(Button));//清空
    button->w=w;
    button->h=h;
    button->color=0x0;//黑色
    button->background=0x00e1e2e3;
    button->fontsize=16;
    return button;
}
//销毁按钮
void  button_destroy(Button* button){
    if(button != NULL)
    {
        if(button->image != NULL)
        {
            free(button->image);
        }
        if(button->text != NULL)
        {
            free(button->text);
        }
        free(button);
    }
}
//设置字体颜色
void button_set_color(Button* button, unsigned int color){
    if(button == NULL) return;
    button->color = color;
}
//设置文字
void button_set_text(Button* button, const char *text){
    if(button == NULL) return;
    if(button->text != NULL)
    {
        free(button->text);
    }
    if(text == NULL)
    {
        button->text==NULL;
        return ;
    }
    //计算text需要的空间
    int size = strlen(text)+1;
    button->text = malloc(size);
    memcpy(button->text, text, size);
}
//设置字体大小
void button_set_fontsize(Button* button, int size){
    if(button == NULL) return;
    button->fontsize = size;
}
//获取字体颜色
unsigned int button_get_color(Button* button){
    if(button == NULL) return 0;
    return button->color;
}
//获取文字
const char * button_get_text(Button* button){
    if(button == NULL) return NULL;
    return button->text;
}
//获取字体大小
int button_get_fontsize(Button* button){
    if(button == NULL) return 0;
    return button->fontsize;
}
//设置背景颜色
void button_set_background(Button* button, unsigned int color){
    if(button == NULL) return;
    button->background = color;
}
//设置背景图片
void button_set_image(Button* button, const char *filename){
    if(button == NULL) return;
    if(button->image != NULL)
    {
        free(button->image);
    }
    //计算text需要的空间
    int size = strlen(filename)+1;
    button->image = malloc(size);
    memcpy(button->image, filename, size);
}
//设置大小
void button_resize(Button* button, int w, int h){
    if(button == NULL) return;
    button->w=w;
    button->h=h;   
}
//设置位置
void button_move(Button* button, int x, int y){
    if(button == NULL) return;
    button->x=x;
    button->y=y;
}