#include "linklist.h"
#include <dirent.h>

#define for_list(head, pos) \
    for (ListNode *pos = head->next; pos != head; pos = pos->next)

#define for_list_plus(head, pos, tmp)                        \
    for (pos = (head)->next, tmp = pos->next; pos != (head); \
         pos = tmp, tmp = pos->next)

// 添加节点逻辑
void __add_list(ListNode *prev, ListNode *node, ListNode *next)
{
    node->next = next;
    node->prev = prev;
    prev->next = node;
    next->prev = node;
}

// 删除节点逻辑
void __del_list(ListNode *prev, ListNode *node, ListNode *next)
{
    prev->next = next;
    next->prev = prev;
    free(node);
    node->prev = node->next = NULL;
}

// 创建节点
ListNode *createlist(typeData *data)
{
    ListNode *node = malloc(sizeof(ListNode)); //创建堆空间
    bzero(node, sizeof(ListNode));             //清空堆空间
    if (data != NULL)                          //数据位不为空复制
        strncpy(node->data, data, 128);
    node->prev = node->next = node; //头头 尾尾相连
    return node;                    //返回节点
}

// 插入节点
bool insertNode(ListNode *head, ListNode *node)
{
    if (head == NULL || node == NULL)
        return false;

    __add_list(head->prev, node, head); //尾插
}

// 删除节点
bool deleteNode(ListNode *pos)
{
    if (pos == NULL) //无法判断是否为头节点
        return false;
    __del_list(pos->prev, pos, pos->next);
}

// 遍历链表
int displayNode(ListNode *head)
{
    if (head == NULL)
        return 0;

    // for_list(head, pos)
    // {
    //     printf("%s\n", pos->data);
    // }
    int n = 0;
    ListNode *pos = head->next;
    while (pos != head)
    {
        printf("%s\n", pos->data);
        pos = pos->next;
        n++;
    }
    printf("链表中一共有%d个文件\n", n);
    return n;
}

// 销毁链表
void destroyNode(ListNode *head)
{
    if (head == NULL)
        return;
    ListNode *tmp;
    ListNode *pos = head->next;
    while (pos != head)
    {
        tmp = pos->next;
        __del_list(pos->prev, pos, pos->next);
        pos = tmp;
    }
}

// int main(int argc, char const *argv[])
// {
//     ListNode *head = createlist(NULL);

//     search_dir("./", ".c", head);
//     destroyNode(head);

//     for_list(head, pos)
//     {
//         printf("%s\n", pos->data);
//     }

//     return 0;
// }
