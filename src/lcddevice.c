#include "lcddevice.h"
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <string.h>
#define ARM

//初始化lcd
LcdDevice *lcd_init(const char *devname)
{
    int fd = open(devname, O_RDWR);
    if (fd < 0)
    {
        perror("open:");
        return NULL;
    }

    //获取lcd信息
    struct fb_var_screeninfo varinfo; // 可变属性
    if (ioctl(fd, FBIOGET_VSCREENINFO, &varinfo) != 0)
    {
        perror("获取LCD设备可变属性信息失败");
        return NULL;
    }

    //申请结构体空间
    LcdDevice *lcd = malloc(sizeof(LcdDevice));
    lcd->fd = fd;
#ifdef ARM
    lcd->width = varinfo.xres;
    lcd->heigth = varinfo.yres;
#else
    lcd->width = varinfo.xres_virtual;
    lcd->heigth = varinfo.yres_virtual;
#endif
    lcd->pixelbyte = varinfo.bits_per_pixel / 8;

    //映射空间
    lcd->mptr = mmap(NULL, lcd->width * lcd->heigth * lcd->pixelbyte,
                     PROT_READ | PROT_WRITE, MAP_SHARED, lcd->fd, 0);
    if (lcd->mptr == (void *)-1)
    {
        perror("mmap:");
        free(lcd);
        return NULL;
    }
    return lcd;
}
//销毁lcd
bool lcd_destroy(LcdDevice *lcd)
{
    if (lcd)
    {
        munmap(lcd->mptr, lcd->width * lcd->heigth * lcd->pixelbyte);
        close(lcd->fd);
        free(lcd);
    }
}
//清屏
void lcd_clear(LcdDevice *lcd, unsigned int color)
{
    //暂存mptr指针
    unsigned int *p = lcd->mptr;
    for (int i = 0; i < lcd->heigth; i++)
    {
        for (int j = 0; j < lcd->width; j++)
        {
            p[j] = color;
        }
        p += lcd->width;
    }
}

// void lcd_draw_line(LcdDevice *lcd, int x, int y, int w, int h)
// {
// }

//绘制矩形
void lcd_draw_rect(LcdDevice *lcd, int x, int y, int w, int h, unsigned int color)
{
    if (lcd == NULL)
        return;
    unsigned int *p = lcd->mptr + x + y * lcd->width;
    unsigned int *q = p;
    // 上边框
    for (int i = 0; i < w; i++)
    {
        p[i] = color;
    }
    // 左边框
    for (int i = 0; i < h; i++)
    {
        p[i * lcd->width] = color;
    }
    q = p + w;
    // 右边框
    for (int i = 0; i < h; i++)
    {
        q[i * lcd->width] = color;
    }
    q = p + h * lcd->width;
    // 下边框
    for (int i = 0; i < w; i++)
    {
        q[i] = color;
    }
}
void lcd_draw_full(LcdDevice *lcd, int x, int y, int w, int h, unsigned int color)
{
    if (lcd == NULL)
        return;
    unsigned int *p = lcd->mptr + x + y * lcd->width;
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            p[j] = color;
        }
        p += lcd->width;
    }
}

inline void lcd_draw_point(LcdDevice *lcd, int x, int y, int w, unsigned int color)
{
    lcd_draw_full(lcd, (x - w / 2 > 0) ? (x - w / 2) : 0, (y - w / 2 > 0) ? (y - w / 2) : 0, w, w, color);
}

//绘制直线
void lcd_draw_line(LcdDevice *lcd, int x, int y, int x1, int y1, int linew)
{
    if (lcd == NULL)
        return;
    unsigned int *p = lcd->mptr + x + y * lcd->width;
    for (int i = 0; i < abs(y - y1); i++)
    {
        int ix = abs(y1 - y) * 100 / abs(x1 - x) * i / 100;
        // p[ix] = 0xff0000;
        p += lcd->width;
        lcd_draw_point(lcd, ix + x, i + y, linew, 0xff0000);
    }
}
//绘制圆形
void lcd_draw_circle(LcdDevice *lcd, int x, int y, int r)
{
}

// 显示文字
// 参数:  lcd描述符 起始位置坐标 图片像素数据 图片长宽
void lcd_show_font(struct LcdDevice *lcd,
                   int x, int y,
                   const char *rgbData,
                   int w, int h)
{
    unsigned int *p = lcd->mptr + x + y * lcd->width;
    char *mp = (char *)rgbData;
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            // BGRA 偏移
            unsigned char tmp[3];
            memcpy(tmp, mp + j * 3, 3);
            unsigned int data = tmp[0];
            data = (data << 8) | (tmp[1]);
            data = (data << 8) | (tmp[2]);
            p[j] = data;
        }
        p += lcd->width;
        mp += w * 3;
    }
}

// 显示原图片
// 参数:  lcd描述符 起始位置坐标 图片像素数据 图片长宽
void lcd_show_bmp(struct LcdDevice *lcd,
                  int x, int y,
                  const char *rgbData,
                  int w, int h)
{
    unsigned int *p = lcd->mptr + x + y * lcd->width;
    char *mp = (char *)rgbData;
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            // RGB没偏移
            memcpy(p + j, mp + j * 3, 3);
        }
        p += lcd->width;
        mp += w * 3;
    }
}
