#include "bmp.h"
#include <string.h>
#include "jpeglib.h"
#include "errno.h"
//创建BmpImage 参数：路径
BmpImage *bmp_create(const char *bmpfile)
{
    //打开文件
    int fd = open(bmpfile, O_RDWR);
    if (fd < 0)
    {
        perror("open:");
        return NULL;
    }

    BmpImage *bmp = malloc(sizeof(BmpImage));
    //读取文件头
    struct bitmap_header bmpheader;
    read(fd, &bmpheader, sizeof(struct bitmap_header));

    //显示bmp头信息
    // bmp_header_show(&bmpheader);
    //解析宽高
    bmp->width = bmpheader.width;
    bmp->heigth = bmpheader.height;
    bmp->pixelbyte = bmpheader.bit_count / 8;

    //分配存储图片数据的空间
    bmp->image = malloc(bmp->width * bmp->heigth * bmp->pixelbyte);
    //定义一行像素占用有效字节数
    int pixelrowsize = bmp->width * bmp->pixelbyte;
    //定义一行数据在文件中占用的字节数(4字节倍数)
    int pad = ((4 - pixelrowsize % 4)) % 4;
    int filerowsize = pixelrowsize + pad;
    //定义空间保存从文件中读取的一行像素数据（包括补齐的字节数）
    char readlinebuffer[filerowsize];
    for (int i = 0; i < bmp->heigth; i++)
    {
        read(fd, readlinebuffer, filerowsize);
        //把读取的数据拷贝到image中 （不拷贝补齐的字节）
        memcpy(bmp->image + pixelrowsize * i, readlinebuffer, pixelrowsize);
    }
    close(fd);
    return bmp;
}
//销毁BmpImage
bool bmp_destroy(BmpImage *bmp)
{
    if (bmp != NULL)
    {
        free(bmp->image);
        free(bmp);
    }
}
//图片盘转
void bmp_reversal(BmpImage *bmp)
{
    if (bmp == NULL)
        return;
    int pixelrowsize = bmp->width * bmp->pixelbyte;
    //定义一个暂存一行数据的空间
    char rowbuffer[pixelrowsize];
    for (int i = 0; i <= bmp->heigth / 2; i++)
    {
        memcpy(rowbuffer, bmp->image + i * pixelrowsize, pixelrowsize); //保存一行
        memcpy(bmp->image + i * pixelrowsize, bmp->image + (bmp->heigth - i - 1) * pixelrowsize, pixelrowsize);
        memcpy(bmp->image + (bmp->heigth - i - 1) * pixelrowsize, rowbuffer, pixelrowsize);
    }
}
//图片缩放
void bmp_zoom(BmpImage *bmp, int destw, int desth)
{
    if (bmp == NULL)
        return;
    int srcw = bmp->width;  //原图宽
    int srch = bmp->heigth; //原图的高
    //目标图的， 宽，高， 空间
    char *destImage = malloc(destw * desth * bmp->pixelbyte);
    for (int y = 0; y < desth; y++)
    {
        //从原图中拷贝像素到目标图中
        for (int x = 0; x < destw; x++)
        {
            destImage[x * 3 + 0 + y * destw * 3] = bmp->image[(srcw * x / destw) * 3 + 0 + (srch * y / desth) * srcw * 3];
            destImage[x * 3 + 1 + y * destw * 3] = bmp->image[(srcw * x / destw) * 3 + 1 + (srch * y / desth) * srcw * 3];
            destImage[x * 3 + 2 + y * destw * 3] = bmp->image[(srcw * x / destw) * 3 + 2 + (srch * y / desth) * srcw * 3];
        }
    }
    //把原图的空间释放， 宽高重新赋值
    bmp->width = destw;
    bmp->heigth = desth;
    free(bmp->image);
    bmp->image = destImage;
}
//保存bmp图片
void bmp_save(BmpImage *bmp, const char *filename)
{
    //定义bmp图片头， 并且初始化里面的数据
    struct bitmap_header header;
    memcpy(&header.type, "BM", 2); // bm
    header.reserved1 = 0;
    header.reserved2 = 0;
    header.offbits = 54;
    header.struct_size = 40;
    header.width = bmp->width;
    header.height = bmp->heigth;
    header.planes = 1;
    header.bit_count = bmp->pixelbyte * 8;
    header.compression = 0;

    int pixelrowsize = bmp->width * bmp->pixelbyte;
    //定义一行数据在文件中占用的字节数(4字节倍数)
    int pad = ((4 - pixelrowsize % 4)) % 4;
    int filerowsize = pixelrowsize + pad;
    header.size_img = filerowsize * bmp->heigth;
    header.size = header.size_img + 54;

    int fd = open(filename, O_WRONLY | O_CREAT, 0777);
    //写头
    write(fd, &header, 54);
    //一行一行写像素值
    for (int i = 0; i < bmp->heigth; i++)
    {
        write(fd, bmp->image + (i * bmp->width * 3), filerowsize);
    }

    close(fd);
}

void bmp_header_show(struct bitmap_header *header)
{
    printf("type=%s\n", (char *)&header->type); // BM
    printf("size=%d\n", header->size);          //图像文件大小 54+像素大小
    printf("reserved1=%d\n", header->reserved1);
    printf("reserved2=%d\n", header->reserved2);
    printf("offbits=%d\n", header->offbits);         // bmp图像数据偏移量
    printf("struct_size=%d\n", header->struct_size); // 本结构大小
    printf("width=%d\n", header->width);             // 图像宽
    printf("height=%d\n", header->height);           // 图像高
    printf("planes=%d\n", header->planes);
    printf("bit_count=%d\n", header->bit_count);     // 色深24
    printf("compression=%d\n", header->compression); //赋值为0
    printf("size_img=%d\n", header->size_img);       // bmp数据大小，必须是4的整数倍
    printf("X_pel=%d\n", header->X_pel);
    printf("Y_pel=%d\n", header->Y_pel);
    printf("clrused=%d\n", header->clrused);
    printf("clrImportant=%d\n", header->clrImportant);
}

BmpImage *jpg_to_rgb(const char *jpgfile)
{
    //打开文件
    int fd = open(jpgfile, O_RDWR);
    if (fd < 0)
    {
        perror("open:");
        return NULL;
    }

    // lseek
    long jpgsize = lseek(fd, 0, SEEK_END); //把文件位置移动到末尾，计算出文件大小
    lseek(fd, 0, SEEK_SET);
    //创建空间存储jpg数据
    char jpgdata[jpgsize];
    //读取数据存储到数组中
    int ret = read(fd, jpgdata, jpgsize);
    if (ret < 0)
        return NULL;

    BmpImage *bmp = malloc(sizeof(BmpImage));

    // 1，声明解码结构体，以及错误管理结构体
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;

    // 2，使用缺省的出错处理来初始化解码结构体
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);

    // 3，配置该cinfo，使其从 jpgdata 中读取jpgsize个字节
    //    这些数据必须是完整的JPEG数据
    jpeg_mem_src(&cinfo, jpgdata, jpgsize);

    // 4，读取JPEG文件的头，并判断其格式是否合法
    if (!jpeg_read_header(&cinfo, true))
    {
        fprintf(stderr, "jpeg_read_header failed: "
                        "%s\n",
                strerror(errno));
    }

    // 5，开始解码
    jpeg_start_decompress(&cinfo);
    // 6，获取图片的尺寸信息
    printf("宽：  %d\n", cinfo.output_width);
    printf("高：  %d\n", cinfo.output_height);
    printf("色深：%d\n", cinfo.output_components);

    // 7，根据图片的尺寸大小，分配一块相应的内存rgbdata
    //    用来存放从jpgdata解码出来的图像数据
    unsigned long linesize = cinfo.output_width * cinfo.output_components;
    unsigned long rgbsize = linesize * cinfo.output_height;
    char *rgbdata = calloc(1, rgbsize);

    bmp->width = cinfo.output_width;
    bmp->heigth = cinfo.output_height;
    bmp->pixelbyte = cinfo.output_components;
    bmp->image = rgbdata;

    // 8，循环地将图片的每一行读出并解码到rgb_buffer中
    int line = 0;
    while (cinfo.output_scanline < cinfo.output_height)
    {
        unsigned char *buffer_array[1];
        buffer_array[0] = rgbdata + cinfo.output_scanline * linesize;
        jpeg_read_scanlines(&cinfo, buffer_array, 1);
    }

    // 9，解码完了，将jpeg相关的资源释放掉
    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);

    return bmp;
}