#ifndef LCDDEVICE_H
#define LCDDEVICE_H

#include "stdio.h"
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef struct LcdDevice
{
    int fd;             //文件描述符
    unsigned int *mptr; //保存映射空间首地址
    int width;          //像素宽
    int heigth;         //像素高
    int pixelbyte;      //一个像素占用的字节
} LcdDevice;

//初始化lcd
LcdDevice *lcd_init(const char *devname);
//销毁lcd
bool lcd_destroy(LcdDevice *lcd);
//清屏
void lcd_clear(LcdDevice *lcd, unsigned int color);
//绘制矩形
void lcd_draw_rect(LcdDevice *lcd, int x, int y, int w, int h, unsigned int color);
void lcd_draw_full(LcdDevice *lcd, int x, int y, int w, int h, unsigned int color);
//绘制直线
void lcd_draw_line(LcdDevice *lcd, int x, int y, int x1, int y1, int linew);
//绘制圆形
void lcd_draw_circle(LcdDevice *lcd, int x, int y, int r);
void lcd_draw_point(LcdDevice *lcd, int x, int y, int w, unsigned int color);
// 显示文字到lcd
void lcd_show_font(struct LcdDevice *lcd, int x, int y, const char *rgbData, int w, int h);
// 显示bmp图片到lcd
void lcd_show_bmp(struct LcdDevice *lcd,
                  int x, int y,
                  const char *rgbData,
                  int w, int h);
#endif