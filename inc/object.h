#ifndef OBJECT_H
#define OBJECT_H

#include "lcddevice.h"
#include "button.h"
#include "font.h"
#include "bmp.h"
#include "tsevent.h"
#include <pthread.h>
#include <dirent.h>
#include <time.h>
#include <termios.h>
#include "terminal_enter.h"
#include "linklist.h"

//显示按钮
void button_show(LcdDevice *lcd, Button *button);
//遍历目录
void search_dir(const char *dname, const char *type, ListNode *head);
// 功能：将文字 上下自定义，左右自定义 显示到lcd
// 参数：lcd描述符 画板指针 文字x坐标 文字y坐标
void button_show_x_y(LcdDevice *lcd, Button *button, int font_pos_x, int font_pos_y);
// 功能：使用画板显示文字
// 参数：文字内容 起始坐标(x,y) 文字大小 文字颜色 文字背景颜色 是否开启居中自定义(如果为false,后两个参数随意填写) 文字相对于画板的坐标x(左右居中请用button_show) 文字相对于画板的坐标y(上下居中y=(画板高-文字大小)/2)
void show_font(char *text,
               int x, int y,
               int w, int h,
               int font_size, int font_color,
               int bgcolor,
               int flag,
               int font_pos_x, int font_pos_y,
               LcdDevice *lcd);

// 功能：显示处理过的图片到lcd屏幕
// 参数：lcd描述符 文件路径 图片缩放大小 图片像素数据bmp->image,图片长宽
void bmp_show(LcdDevice *lcd, const char *bmpfile, int x, int y, int w, int h);

#endif // OBJECT_H