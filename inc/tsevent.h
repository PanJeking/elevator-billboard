#ifndef TSEVENT_H
#define TSEVENT_H

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/input.h> // 系统定义输入设备操作的API

// 获取触摸屏坐标的开关
extern int ts_flag;

typedef struct Point
{
    int x, y;
} Point;

typedef struct TsDevice
{
    int fd;
    struct input_event inputData;
} TsDevice;

// 初始化
struct TsDevice *ts_init(const char *devname);
// 销毁触摸屏
bool ts_destroy(struct TsDevice *ts);
//获取坐标点
Point ts_point(struct TsDevice *ts);

#endif