#ifndef _TERMINAL_ENTER_H
#define _TERMINAL_ENTER_H

#include <stdio.h>
#include <string.h>
#include <termios.h>

// 修改终端的输入模式为行缓冲（需要按回车才能获取到标准输入中的内容）
void terminal_Enter_mode(void);

// 修改终端的输入模式为单缓冲(不需要按回车就可以获取键盘键入内容)
void terminal_notEnter_mode(void);

#endif //_TERMINAL_ENTER_H