#ifndef BUTTON_H
#define BUTTON_H

#include "stdio.h"
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

typedef struct Button
{
    int x, y;
    int w, h;
    char *text;
    char *image;
    unsigned int background;
    unsigned int color;
    int fontsize;
} Button;

//创建按钮
Button *button_create(int w, int h);
//销毁按钮
void button_destroy(Button *button);
//设置字体颜色
void button_set_color(Button *button, unsigned int color); // BGRA
//设置文字
void button_set_text(Button *button, const char *text);
//设置字体大小
void button_set_fontsize(Button *button, int size);
//获取字体颜色
unsigned int button_get_color(Button *button);
//获取文字
const char *button_get_text(Button *button);
//获取字体大小
int button_get_fontsize(Button *button);
//设置背景颜色
void button_set_background(Button *button, unsigned int color); // BGRA
//设置背景图片
void button_set_image(Button *button, const char *filename);
//设置大小
void button_resize(Button *button, int w, int h);
//设置位置
void button_move(Button *button, int x, int y);
//显示
#endif // BUTTON_H