#ifndef _LINKLIST_H_
#define _LINKLIST_H_

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#define typeData char

// 创建链表结构体
typedef struct ListNode
{
    typeData data[128];    //数据
    struct ListNode *prev; //前驱
    struct ListNode *next; //后继
} ListNode;

// 添加节点逻辑
void __add_list(ListNode *prev, ListNode *node, ListNode *next);

// 删除节点逻辑
void __del_list(ListNode *prev, ListNode *node, ListNode *next);

// 创建节点
ListNode *createlist(typeData *data);
// 插入节点
bool insertNode(ListNode *head, ListNode *node);
// 删除节点
bool deleteNode(ListNode *pos);
// 遍历节点
int displayNode(ListNode *head);
// 销毁链表
void destroyNode(ListNode *head);

#endif // _LINKLIST_H_