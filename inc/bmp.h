#ifndef BMP_H
#define BMP_H
#include "stdio.h"
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef short int16_t;
typedef int int32_t;

// BMP格式头规范
struct bitmap_header
{
	int16_t type;
	int32_t size; // 图像文件大小
	int16_t reserved1;
	int16_t reserved2;
	int32_t offbits;	 // bmp图像数据偏移量
	int32_t struct_size; // 本结构大小
	int32_t width;		 // 图像宽
	int32_t height;		 // 图像高
	int16_t planes;
	int16_t bit_count; // 色深
	int32_t compression;
	int32_t size_img; // bmp数据大小，必须是4的整数倍
	int32_t X_pel;
	int32_t Y_pel;
	int32_t clrused;
	int32_t clrImportant;
} __attribute__((packed));

//设计一个bmp图片的结构体
typedef struct BmpImage
{
	int32_t width;
	int32_t heigth;
	int8_t pixelbyte; //一个像素占用字节数3，4
	int8_t *image;
} BmpImage;
//创建BmpImage
BmpImage *bmp_create(const char *bmpfile);

BmpImage *jpg_to_rgb(const char *jgpfile);

//销毁BmpImage
bool bmp_destroy(BmpImage *bmp);
//图片盘转
void bmp_reversal(BmpImage *bmp);
//图片缩放
void bmp_zoom(BmpImage *bmp, int w, int h);
//保存bmp图片
void bmp_save(BmpImage *bmp, const char *filename);
void bmp_header_show(struct bitmap_header *header);

#endif // BMP_H