#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>

/*
	更详细的参考 man 3 tcgetattr

	ICANON
	Enable canonical mode. This enables the special characters EOF, EOL, EOL2, ERASE, KILL, LNEXT, REPRINT, STATUS, and WERASE, and buffers by lines.

	可以看出ICNAON标志位启用了整行缓存。

	所以，new_settings.c_lflag &= (~ICANON);这句屏蔽整行缓存。那就只能单个了。
*/

// 修改终端的输入模式为行缓冲（需要按回车才能获取到标准输入中的内容）
void terminal_Enter_mode(void)
{
	struct termios termios_p;
	bzero(&termios_p, sizeof(struct termios));

	tcgetattr(stdin->_fileno, &termios_p); // 获取终端的xxx属性
	printf("设置前 termios_p.c_lflag = %d \n", termios_p.c_lflag);
	termios_p.c_lflag |= (ICANON); // 修改属性，使用行缓冲
	printf("设置后 termios_p.c_lflag = %d \n", termios_p.c_lflag);
	tcsetattr(stdin->_fileno, TCSANOW, &termios_p); // 设置终端的xxx属性
}
// 修改终端的输入模式为单缓冲（不需要按回车就可以获取键盘键入的内容）
void terminal_notEnter_mode(void)
{
	struct termios termios_p;
	bzero(&termios_p, sizeof(struct termios));

	tcgetattr(stdin->_fileno, &termios_p); // 获取终端的xxx属性
	printf("设置前 termios_p.c_lflag = %d \n", termios_p.c_lflag);
	termios_p.c_lflag &= (~ICANON); // 修改属性，屏蔽整行缓存
	printf("设置后 termios_p.c_lflag = %d \n", termios_p.c_lflag);
	tcsetattr(stdin->_fileno, TCSANOW, &termios_p); // 设置终端的xxx属性
}

int main(int argc, char const *argv[])
{

	/*** 验证修改后的标准输入缓冲状态 ***/
	// 只能获取一个字符类型数据
	// 修改终端的输入模式（不需要按回车就可以获取键盘键入的内容）
	terminal_notEnter_mode();
	// 输入字符串一定要敲回车, scanf才能返回
	printf("00  %c \n", getchar());

	/*** 验证修改后的标准输入缓冲状态 ***/
	// 只能获取一个字符类型数据
	// 修改终端的输入模式（不需要按回车就可以获取键盘键入的内容）
	terminal_notEnter_mode();
	// 输入字符串一定要敲回车, scanf才能返回
	char ch;
	scanf("%c", &ch);
	printf("01  %c \n", ch);

	// // 修改终端的输入模式（不需要按回车就可以获取键盘键入的内容）
	// terminal_notEnter_mode();

	// // 输入字符串一定要敲回车, scanf才能返回
	// char str[128];
	// scanf("%s", str);
	// while (getchar() != '\n');	// 清空标准输入的缓冲区
	// printf("--%s \n", str);

	// // 输入字符串一定要敲回车, scanf才能返回
	// printf("---%c \n", getchar());
	// // while (getchar() != '\n');	// 清空标准输入的缓冲区

	// // 输入整型数据一定要敲回车, scanf才能返回
	// int num;
	// scanf("%d", &num);
	// printf("----%d \n", num);

	return 0;
}
