#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

void *routine(void *arg)
{
	char *str = malloc(128);	// 申请堆空间
	bzero(str, 128);			// 清空堆空间

	memcpy(str, "password", 128);	// 内存拷贝，最多拷贝128字节

	return	(void *)str;
}

int main(int argc, char const *argv[])
{
	pthread_t thread_ID;
	int err = pthread_create(&thread_ID, NULL, routine, NULL);	// 创建线程
	if (err != 0)	// 出错判断
		fprintf(stderr, "pthread_create  error : %s \n", strerror(err));// 出错了，打印错误号码的信息

	char *thread_return;	// 定义一个指针，接收线程返回的堆空间地址
	pthread_join(thread_ID, (void **)&thread_return);	// 阻塞等待接合线程，把指针给函数，让该函数帮我指向线程返回的堆空间地址，并且回收线程资源

	printf("thread_return = %s \n", thread_return);

	return 0;
}
